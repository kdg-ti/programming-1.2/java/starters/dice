package be.kdg.dice.view;

import be.kdg.dice.model.Dice;

public class Presenter {
    private Dice model;
    private DiceView view;

    public Presenter(Dice model, DiceView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        //TODO
    }

    private void updateView() {
        //TODO
    }
}
